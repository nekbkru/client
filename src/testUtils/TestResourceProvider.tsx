import React, { ReactNode, ReactElement } from "react";
import { MemoryRouter } from "react-router-dom";
import StoreProvider from "../states/StoreProvider";
import ThemeProvider from "../ui/theme/ThemeProvider";
import dummies from "./TestDummies";

interface Props {
  children?: ReactNode;
}

/**
 * Wrapper with router, store provider and theme provider for app tests.
 */
function TestResourceProvider(props: Props): ReactElement {
  return (
    <StoreProvider {...dummies}>
      <ThemeProvider {...dummies}>
        <MemoryRouter>{props.children}</MemoryRouter>
      </ThemeProvider>
    </StoreProvider>
  );
}

export default TestResourceProvider;
