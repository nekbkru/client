import { createBrowserHistory, createLocation } from "history";
import DefaultTheme from "../ui/theme/DefaultTheme";
import { createStore } from "redux";
import { createRootReducer } from "../states/RootReducer";

// Constants for single component (shallow) tests.
// Use it instead of wrapping components in resource providers.

// Router dummies.
const history = createBrowserHistory();
const location = createLocation("/");
const match = {
  params: {},
  isExact: false,
  path: "/",
  url: "dummy.test",
};

// Theme dummies.
const theme = DefaultTheme();

// Redux dummies.
const store = createStore(createRootReducer());

export default { history, location, match, theme, store };
