import React, { ReactNode, ReactElement } from "react";
import { ThemeProvider as StyledThemeProvider } from "styled-components";
import { MuiThemeProvider, Theme } from "@material-ui/core/styles";
import { StylesProvider } from "@material-ui/styles";
import CssBaseline from "@material-ui/core/CssBaseline";

import DefaultTheme from "./DefaultTheme";
interface ThemeProviderProps {
  theme?: Theme;
  children: ReactNode;
}

/**
 * Wrapper for mui and styled-components theme providers.
 *
 * @param theme overrides default theme (not required).
 * @param children theme applies to this element(s).
 */
function ThemeProvider(props: ThemeProviderProps): ReactElement {
  const nextTheme = Object.assign({}, DefaultTheme(), props.theme);

  return (
    <StylesProvider injectFirst>
      <CssBaseline />
      <StyledThemeProvider theme={nextTheme}>
        <MuiThemeProvider theme={nextTheme}>{props.children}</MuiThemeProvider>
      </StyledThemeProvider>
    </StylesProvider>
  );
}

ThemeProvider.defaultProps = {
  theme: DefaultTheme(),
};

export default ThemeProvider;
