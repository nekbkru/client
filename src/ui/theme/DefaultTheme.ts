import { createMuiTheme } from "@material-ui/core/styles";
import defaultThemeRes from "./defaultThemeRes.json";
import { ThemeOptions, Theme } from "@material-ui/core/styles/createMuiTheme";

/**
 * Default theme creator.
 */
function DefaultTheme(): Theme {
  return createMuiTheme(defaultThemeRes as ThemeOptions);
}

export default DefaultTheme;
