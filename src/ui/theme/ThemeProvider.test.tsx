import React from "react";
import { Theme, createMuiTheme } from "@material-ui/core/styles";
import styled from "styled-components";
import ThemeProvider from "./ThemeProvider";
import defaultThemeRes from "./defaultThemeRes.json";
import TestRenderer from "react-test-renderer";

let testColor = "";

const StyledDiv = styled.div`
  color: ${(props): string => {
    testColor = props.theme.palette.primary.main;
    return testColor;
  }};
`;

const themeProps = {
  palette: {
    primary: {
      main: "#010",
    },
  },
};

const theme: Theme = createMuiTheme(themeProps);

it("Theme provider works correctly", (): void => {
  TestRenderer.create(
    <ThemeProvider>
      <StyledDiv>Some text</StyledDiv>
    </ThemeProvider>,
  );
  // default theme primary color.
  expect(testColor).toBe(defaultThemeRes.palette.primary.main);

  TestRenderer.create(
    <ThemeProvider theme={theme}>
      <StyledDiv>Some text</StyledDiv>
    </ThemeProvider>,
  );
  // test theme primary color.
  expect(testColor).toBe(themeProps.palette.primary.main);
});
