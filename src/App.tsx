import React, { ReactElement } from "react";
import MainPageSwitch from "./mainPage/MainPageSwitch";
import { Switch, Route, Redirect, RouteComponentProps, withRouter } from "react-router-dom";
import OldMainPage from "./mainPage/OldMainPage";
import MainPage from "./mainPage/MainPage";

interface Props {
  isOldPage: boolean;
  onSwitch: () => void;
}

export const OLD_MAIN_PAGE_PATH = "/old-korona-lift";
export const NEW_MAIN_PAGE_PATH = "/korona-lift";

/**
 * Page container.
 *
 * @param isOldPage is old main page enabled.
 * @param onSwitch called on click switch element.
 */
class App extends React.Component<Props & RouteComponentProps> {
  private prevNewMainPagePath = NEW_MAIN_PAGE_PATH;
  private prevOldMainPagePath = OLD_MAIN_PAGE_PATH;

  private switchPage(oldPage: boolean): ReactElement | null {
    let redirect = null;
    if (this.props.location.pathname.startsWith(OLD_MAIN_PAGE_PATH) && !oldPage) {
      redirect = <Redirect to={this.prevNewMainPagePath} />;
      this.prevOldMainPagePath = this.props.location.pathname;
    }
    if (this.props.location.pathname.startsWith(NEW_MAIN_PAGE_PATH) && oldPage) {
      redirect = <Redirect to={this.prevOldMainPagePath} />;
      this.prevNewMainPagePath = this.props.location.pathname;
    }
    return redirect;
  }

  public render(): ReactElement {
    return (
      <div>
        <MainPageSwitch onSwitch={this.props.onSwitch} checked={this.props.isOldPage} />

        <Switch>
          <Route exact path="/" render={(): ReactElement => <Redirect to={this.props.isOldPage ? OLD_MAIN_PAGE_PATH : NEW_MAIN_PAGE_PATH} />} />
          <Route path={NEW_MAIN_PAGE_PATH} component={MainPage} />
          <Route path={OLD_MAIN_PAGE_PATH} component={OldMainPage} />
          <Route render={(): ReactElement => <div>Error 404 Page not found</div>} />
        </Switch>
        {this.switchPage(this.props.isOldPage)}
      </div>
    );
  }
}

export default withRouter(App);
