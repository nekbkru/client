import React from "react";
import ChangelogWindow, { CHANGELOG_HEADER } from "./ChangelogWindow";
import TestResourceProvider from "../testUtils/TestResourceProvider";
import { render, fireEvent } from "@testing-library/react";
import dummies from "../testUtils/TestDummies";
import { CLOSE_BUTTON_TITLE } from "../window/Window";

it("Check changelog panel rendered correctly", (): void => {
  const spy = jest.spyOn(dummies.history, "goBack");
  const wrapper = render(
    <TestResourceProvider>
      <ChangelogWindow {...dummies} />
    </TestResourceProvider>,
  );
  expect(wrapper.getByText(CHANGELOG_HEADER)).toBeDefined();
  expect(spy).not.toHaveBeenCalled();
  fireEvent.click(wrapper.getByTitle(CLOSE_BUTTON_TITLE));
  expect(spy).toHaveBeenCalled();
});
