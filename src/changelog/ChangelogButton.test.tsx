import React from "react";
import ChangelogButton, { CANGELOG_BUTTON_TEXT } from "./ChangelogButton";
import TestResourceProvider from "../testUtils/TestResourceProvider";
import { fireEvent } from "@testing-library/dom";
import { render } from "@testing-library/react";
import { CHANGELOG_HEADER } from "./ChangelogWindow";

it("Test changelog button", (): void => {
  const wrapper = render(<ChangelogButton />, { wrapper: TestResourceProvider });
  const button = wrapper.getByText(CANGELOG_BUTTON_TEXT);
  fireEvent.click(button);
  const window = wrapper.getByText(CHANGELOG_HEADER);
  expect(window).toBeTruthy();
});
