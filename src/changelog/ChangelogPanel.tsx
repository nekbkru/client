import React, { ReactElement } from "react";
import changelogLoader from "./changelogLoader";
import ReactMarkdown from "react-markdown";

interface State {
  changelog: string;
}

/**
 * Panel to render changelog.
 */
class ChangelogPanel extends React.Component<object, State> {
  public constructor(props: object) {
    super(props);
    this.state = { changelog: "" };
  }

  public componentDidMount(): void {
    changelogLoader().then((text): void => {
      const clearText = this.removeComments(text);
      this.setState({ changelog: clearText });
    });
  }

  private removeComments(text: string): string {
    return text.replace(/(?=<!--)([\s\S]*?)-->/g, "");
  }

  public render(): ReactElement {
    return <ReactMarkdown source={this.state.changelog} />;
  }
}

export default ChangelogPanel;
