export const MOCK_CLEAR_TEXT = "TestChangelogText";
const fullText = `<!-- some comment -->${MOCK_CLEAR_TEXT}<!-- two line 
                                                                       comment -->`;

async function changelogLoader(): Promise<string> {
  return Promise.resolve(fullText);
}

export default changelogLoader;
