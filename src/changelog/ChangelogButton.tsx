import React, { ReactElement } from "react";
import packageInfo from "../../package.json";
import styled from "styled-components";
import { Button } from "@material-ui/core";
import { Route } from "react-router-dom";
import { RouteComponentProps, withRouter } from "react-router";
import ChangelogWindow from "./ChangelogWindow";

export const CANGELOG_BUTTON_TEXT = "KCS v" + packageInfo.version;

const StyledButton = styled(Button)`
  text-transform: none;
`;

/**
 * Changelog button.
 */
class ChangelogButton extends React.Component<RouteComponentProps> {
  private changelogPath = `${this.props.match.path}/changelog`;

  private onClickChangelobButton(): void {
    this.props.history.push(this.changelogPath);
  }

  public render(): ReactElement {
    return (
      <div>
        <StyledButton variant="outlined" onClick={(): void => this.onClickChangelobButton()}>
          {CANGELOG_BUTTON_TEXT}
        </StyledButton>
        <Route path={this.changelogPath} component={ChangelogWindow} />
      </div>
    );
  }
}

export default withRouter(ChangelogButton);
