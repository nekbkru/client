/**
 * Component that loads changelog;
 */
async function changelogLoader(): Promise<string> {
  const changelogPath = "/CHANGELOG.md";
  return fetch(changelogPath)
    .then((res): Promise<string> => res.text())
    .then((text): string => {
      return text;
    });
}

export default changelogLoader;
