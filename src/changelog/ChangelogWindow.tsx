import React, { ReactElement } from "react";
import { RouteComponentProps } from "react-router";
import Window from "../window/Window";
import ChangelogPanel from "./ChangelogPanel";

export const CHANGELOG_HEADER = "История изменений";

/**
 * Window with application changelog.
 */
class ChangelogWindow extends React.Component<RouteComponentProps> {
  private closeModal(): void {
    this.props.history.goBack();
  }

  public render(): ReactElement {
    return (
      <Window header={CHANGELOG_HEADER} onClose={(): void => this.closeModal()}>
        <ChangelogPanel />
      </Window>
    );
  }
}

export default ChangelogWindow;
