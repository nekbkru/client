import React from "react";
import { render, wait } from "@testing-library/react";
import dummies from "../testUtils/TestDummies";
import { MOCK_CLEAR_TEXT } from "./__mocks__/changelogLoader";
import ChangelogPanel from "./ChangelogPanel";

jest.mock("./changelogLoader");

it("Check changelog panel rendered correctly", async (): Promise<void> => {
  const wrapper = render(<ChangelogPanel {...dummies} />);
  await wait((): void => {
    expect(wrapper.getByText(MOCK_CLEAR_TEXT)).toBeDefined();
  });
});
