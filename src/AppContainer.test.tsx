import React from "react";
import AppContainer from "./AppContainer";
import MainPageSwitch from "./mainPage/MainPageSwitch";
import dummies from "./testUtils/TestDummies";
import TestRenderer, { act } from "react-test-renderer";
import TestResourceProvider from "./testUtils/TestResourceProvider";

it("Check switch between old and new main pages", (): void => {
  const mockStore = dummies.store;
  const wrapper = TestRenderer.create(
    <TestResourceProvider>
      <AppContainer {...dummies} />
    </TestResourceProvider>,
  );
  expect(mockStore.getState().mainPageState.isOldMainPage).toBe(true);
  act((): void => {
    wrapper.root.findByType(MainPageSwitch).props.onSwitch();
  });
  expect(mockStore.getState().mainPageState.isOldMainPage).toBe(false);
});
