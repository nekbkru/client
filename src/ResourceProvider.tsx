import React, { ReactNode, ReactElement } from "react";
import { BrowserRouter } from "react-router-dom";
import StoreProvider from "./states/StoreProvider";
import ThemeProvider from "./ui/theme/ThemeProvider";

interface Props {
  children?: ReactNode;
}

/**
 * Wrapper with router, store provider and theme provider for entire app.
 */
function ResourceProvider(props: Props): ReactElement {
  return (
    <StoreProvider>
      <BrowserRouter>
        <ThemeProvider>{props.children}</ThemeProvider>
      </BrowserRouter>
    </StoreProvider>
  );
}

export default ResourceProvider;
