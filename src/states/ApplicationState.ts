import { MainPageState } from "../mainPage/state/MainPageState";

/**
 * Full application state.
 * Should contain states of all components.
 */
export interface ApplicationState {
  mainPageState: MainPageState;
  // Add new states here.
}
