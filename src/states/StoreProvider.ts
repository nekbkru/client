import { ReactNode, ReactElement, createElement } from "react";
import { Provider } from "react-redux";
import { createStore, Store } from "redux";
import { createRootReducer } from "./RootReducer";
import { composeWithDevTools } from "redux-devtools-extension";

interface Props {
  children: ReactNode;
  store?: Store;
}

/**
 * Wrapper element with redux store.
 * @param props children elements.
 */
function StoreProvider(props: Props): ReactElement {
  const browserDevToolsConnect = composeWithDevTools();
  const store = props.store === undefined ? createStore(createRootReducer(), browserDevToolsConnect) : props.store;

  return createElement(Provider, { store }, props.children);
}

export default StoreProvider;
