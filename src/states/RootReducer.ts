import { Reducer, combineReducers } from "redux";
import { mainPageSwitchReducer } from "../mainPage/state/MainPageSwitchReducer";

/**
 * Root reducer used for creating application state.
 * Should contain reducers of all components states.
 */
export const createRootReducer = (): Reducer =>
  combineReducers({
    mainPageState: mainPageSwitchReducer,
    // Add new reducers here.
  });
