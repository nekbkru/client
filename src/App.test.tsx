import React from "react";
import App from "./App";
import OldMainPage from "./mainPage/OldMainPage";
import MainPage from "./mainPage/MainPage";
import dummies from "./testUtils/TestDummies";
import TestRenderer from "react-test-renderer";
import TestResourceProvider from "./testUtils/TestResourceProvider";

const oldPageStateProps = {
  isOldPage: true,
  onSwitch: (): void => {},
};

const newPageStateProps = {
  isOldPage: false,
  onSwitch: (): void => {},
};

it("App renders with correct routes", (): void => {
  const oldPageWrapper = TestRenderer.create(
    <TestResourceProvider>
      <App isOldPage={oldPageStateProps.isOldPage} onSwitch={oldPageStateProps.onSwitch} {...dummies} />
    </TestResourceProvider>,
  );
  expect(oldPageWrapper.root.findAllByType(MainPage).length).toBe(0);
  expect(oldPageWrapper.root.findAllByType(OldMainPage).length).toBe(1);

  const newPageWrapper = TestRenderer.create(
    <TestResourceProvider>
      <App isOldPage={newPageStateProps.isOldPage} onSwitch={newPageStateProps.onSwitch} {...dummies} />
    </TestResourceProvider>,
  );
  expect(newPageWrapper.root.findAllByType(OldMainPage).length).toBe(0);
  expect(newPageWrapper.root.findAllByType(MainPage).length).toBe(1);
});
