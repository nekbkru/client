import React, { ReactElement } from "react";
import styled from "styled-components";
import ChangelogButton from "../changelog/ChangelogButton";

const Content = styled.div`
  border: none;
  ${(props): string => props.theme.typography.h2};
`;

export const CONTENT_TEXT = "Тут могла быть ваша реклама!";

/**
 * New main page element.
 */
function MainPage(): ReactElement {
  return (
    <div>
      <ChangelogButton />
      <Content>{CONTENT_TEXT}</Content>
    </div>
  );
}

export default MainPage;
