import React from "react";
import MainPage from "./MainPage";
import ChangelogButton from "../changelog/ChangelogButton";
import TestRenderer from "react-test-renderer";
import TestResourceProvider from "../testUtils/TestResourceProvider";

it("Check new main page renders correctly", (): void => {
  const wrapper = TestRenderer.create(
    <TestResourceProvider>
      <MainPage />
    </TestResourceProvider>,
  );
  expect(wrapper.root.findByType(ChangelogButton)).toBeTruthy();
});
