import * as constants from "./Constants";

export interface SwitchMainPage {
  type: constants.MAIN_PAGE_ACTION_TYPE;
}

export function switchMainPage(): SwitchMainPage {
  return {
    type: constants.MAIN_PAGE_ACTION_TYPE.SWITCH,
  };
}
