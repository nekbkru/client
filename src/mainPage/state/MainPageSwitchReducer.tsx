import { MainPageState } from "./MainPageState";
import { SwitchMainPage } from "./Actions";
import { MAIN_PAGE_ACTION_TYPE } from "./Constants";
import { NEW_MAIN_PAGE_PATH } from "../../App";

const defaultState: MainPageState = {
  isOldMainPage: !window.location.pathname.startsWith(NEW_MAIN_PAGE_PATH),
};

export function mainPageSwitchReducer(state: MainPageState = defaultState, action: SwitchMainPage): MainPageState {
  switch (action.type) {
    case MAIN_PAGE_ACTION_TYPE.SWITCH:
      return { ...state, isOldMainPage: !state.isOldMainPage };
    default:
      return state;
  }
}
