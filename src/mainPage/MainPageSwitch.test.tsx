import React from "react";
import MainPageSwitch from "./MainPageSwitch";
import { Switch } from "@material-ui/core";
import TestRenderer from "react-test-renderer";

const checked = { isChecked: false };

function onSwitch(): void {
  checked.isChecked = !checked.isChecked;
}

it("MainPageSwitch test", (): void => {
  const wrapper = TestRenderer.create(<MainPageSwitch checked={checked.isChecked} onSwitch={onSwitch} />);
  const pageSwitch = wrapper.root.findByType(Switch);
  pageSwitch.props.onChange();
  expect(checked.isChecked).toBe(true);
  pageSwitch.props.onChange();
  expect(checked.isChecked).toBe(false);
});
