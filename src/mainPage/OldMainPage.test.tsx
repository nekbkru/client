import React from "react";
import OldMainPage from "./OldMainPage";
import TestRenderer from "react-test-renderer";

it("Check old main page renders correctly", (): void => {
  const wrapper = TestRenderer.create(<OldMainPage />);
  expect(wrapper.root.findByType("iframe")).toBeTruthy();
});
