import React, { ReactElement } from "react";
import styled from "styled-components";

const Content = styled.iframe`
  width: 100%;
  height: 100%;
  position: absolute;
  border: none;
`;

export const WARNING_TEXT = "Ваш браузер не поддерживает плавающие фреймы!";

/**
 * Frame with old main page.
 */
function OldMainPage(): ReactElement {
  return <Content src="http://localhost:8080/korona-lift/">{WARNING_TEXT}</Content>;
}

export default OldMainPage;
