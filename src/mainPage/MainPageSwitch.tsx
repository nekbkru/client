import React, { ReactElement } from "react";
import { Switch, FormControlLabel } from "@material-ui/core";
import styled from "styled-components";

const SwitchControlLabel = styled(FormControlLabel)`
  position: relative;
  float: right;
  z-index: 999;
`;

interface Props {
  checked: boolean;
  onSwitch: () => void;
}

export const LABEL_TEXT = "Layout switch";

/**
 * Switch between old and new main page.
 *
 * @param {onSwitch} on change event handler..
 * @param {checked} switch value.
 */
function MainPageSwitch(props: Props): ReactElement {
  return <SwitchControlLabel checked={props.checked} control={<Switch color="primary" onChange={props.onSwitch} />} label={LABEL_TEXT} />;
}

export default MainPageSwitch;
