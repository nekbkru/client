import React, { ReactElement, Component, Dispatch } from "react";
import { ApplicationState } from "./states/ApplicationState";
import { SwitchMainPage, switchMainPage } from "./mainPage/state/Actions";
import App from "./App";
import { connect } from "react-redux";

interface StateProps {
  isOldMainPage: boolean;
}

interface DispatchProps {
  onSwitchMainPage: () => void;
}

/**
 * Application root element.
 */
class AppContainer extends Component<StateProps & DispatchProps> {
  public render(): ReactElement {
    const isOldPage = this.props.isOldMainPage;
    const onSwitch = this.props.onSwitchMainPage;
    return <App isOldPage={isOldPage} onSwitch={onSwitch} />;
  }
}

function mapStateToProps({ mainPageState }: ApplicationState): StateProps {
  return { isOldMainPage: mainPageState.isOldMainPage };
}

function mapDispatchToProps(dispatch: Dispatch<SwitchMainPage>): DispatchProps {
  return { onSwitchMainPage: (): void => dispatch(switchMainPage()) };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AppContainer);
