import { connect, MapStateToPropsParam, MapDispatchToPropsNonObject } from "react-redux";
import { withRouter, WithRouterStatics } from "react-router";
import { ComponentType } from "react";

/**
 * Wrapper for withRouter and connect functions. Fix @types/react-router bug.
 * Use if you need both of this functions in one component.
 * Issue https://github.com/DefinitelyTyped/DefinitelyTyped/issues/18999
 */
/* eslint-disable */
export function withRouterAndRedux<TStateProps = {}, TDispatchProps = {}, TOwnProps = {}, State = {}>(
  mapStateToProps: MapStateToPropsParam<TStateProps, TOwnProps, State>,
  mapDispatchToProps: MapDispatchToPropsNonObject<TDispatchProps, TOwnProps>,
  componetType: Function,
): React.ComponentClass & WithRouterStatics<any> {
  let component: React.ComponentType<any> = connect(
    mapStateToProps,
    mapDispatchToProps,
  )(componetType as ComponentType);
  return withRouter(component);
}
/* eslint-enable */
