import React, { ReactElement } from "react";
import { Modal, IconButton, Typography } from "@material-ui/core";
import { ModalProps } from "@material-ui/core/Modal";
import styled from "styled-components";
import { CloseOutlined } from "@material-ui/icons";

export const CLOSE_BUTTON_TITLE = "Закрыть";

const CloseButton = styled(IconButton)`
  position: absolute;
  top: 0;
  right: 0;
`;

const WindowRootElement = styled.div`
  position: relative;
  padding: 5px;
  margin: auto auto;
  height: 90%;
  width: 90%;
  outline: none;
  background: ${(props): string => props.theme.palette.background.default};
`;

const Container = styled.div`
  width: 100%;
  height: 100%;
  flex-direction: column;
  display: flex;
`;

interface Props extends ModalProps {
  header?: string;
  onClose: VoidFunction;
}

/**
 * Window element. Use it instead of MUI's modal.
 */
class Window extends React.Component<Props> {
  public static defaultProps = {
    header: "",
    open: true,
    children: <div />,
  };

  private onClose(): void {
    if (this.props.onClose !== undefined) {
      this.props.onClose();
    }
  }

  public render(): ReactElement {
    return (
      <Modal {...this.props}>
        <WindowRootElement>
          <CloseButton title={CLOSE_BUTTON_TITLE} onClick={(): void => this.onClose()}>
            <CloseOutlined />
          </CloseButton>
          <Container>
            <Typography variant="h4">{this.props.header}</Typography>
            {this.props.children}
          </Container>
        </WindowRootElement>
      </Modal>
    );
  }
}

export default Window;
