import React from "react";
import Window, { CLOSE_BUTTON_TITLE } from "./Window";
import ThemeProvider from "../ui/theme/ThemeProvider";
import { render, fireEvent } from "@testing-library/react";

it("Default window test", (): void => {
  const closeFunction = jest.fn();
  const headerText = "TestHeaderText";
  const contentText = "TestContentText";

  const wrapper = render(
    <ThemeProvider>
      <Window header={headerText} onClose={closeFunction}>
        <div>{contentText}</div>
      </Window>
    </ThemeProvider>,
  );

  expect(wrapper.getByText(headerText)).toBeDefined();
  expect(wrapper.getByText(contentText)).toBeDefined();
  fireEvent.click(wrapper.getByTitle(CLOSE_BUTTON_TITLE));
  expect(closeFunction).toBeCalledTimes(1);
});
