#!/usr/bin/env bash
firstWord=$(echo "$CI_MERGE_REQUEST_TITLE" | awk '{ print $1 }')

# Get the first letter of the first words.
firstLetter=$(echo "$firstWord" | sed 's/^\(.\).*/\1/')

SYSTEM="System:"

if [ $firstWord == $SYSTEM ]; then
  echo "Request name is correct '$CI_MERGE_REQUEST_TITLE' "
  exit 0
fi

case "$firstLetter" in
[А-Я])
  echo "Request name is correct '$CI_MERGE_REQUEST_TITLE' "
  exit 0
  ;;
esac

echo "The request name '$CI_MERGE_REQUEST_TITLE' must be Russian and starts with a capital letter, or has the prefix '$SYSTEM' "
exit 1
