#!/usr/bin/env bash
DEVELOP_CHANGELOG_FILE='CHANGELOG.md'
EXCLUDE_PREFIX='System:\|system:'

GENERATION_DATE_LINE_PREFIX='<!-- Time stamp: '
GENERATION_DATE_LINE_SUFFIX=' -->'
GENERATION_DATE_EXTRACT_PATTERN='(?<='${GENERATION_DATE_LINE_PREFIX}').*?(?='${GENERATION_DATE_LINE_SUFFIX}')'

GENERATION_DATE_FORMAT='%s'
GITLAB_API_DATE_FORMAT='%Y-%m-%dT%H:%M:%S'
SHOWED_CHANGE_DATE_PATTERN='%Y-%m-%d'

JQ_CHANGES_PATH='.[].title'

# Initialize values if changelog isn't still exist.
touch ${DEVELOP_CHANGELOG_FILE}
previousDate="2000-01-01"


# Get last generation date from top line.
oldFirstLine=$(head -n 1 ${DEVELOP_CHANGELOG_FILE})
if [ "${oldFirstLine}" ]
then
    previousDate=$(echo "${oldFirstLine}" | grep -P "${GENERATION_DATE_EXTRACT_PATTERN}" -o)
fi
formattedPreviousDate=$(date -d "@${previousDate}" +"${GITLAB_API_DATE_FORMAT}") # Prefix '@' need for parse timestamp.

# Get last changes from last merged request.
mergedRequestsInfoResponse=$(curl -f -X GET -G -H "Private-Token: ${PUSH_TOKEN}" "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/merge_requests" -d target_branch=master -d state=merged -d updated_after=${formattedPreviousDate}) || exit 1
changes=$(echo -e ${mergedRequestsInfoResponse} | jq -r "${JQ_CHANGES_PATH}" | grep -v "${EXCLUDE_PREFIX}" | uniq | sed -e 's/^/- /')


if [ "${changes}" ]
then

    # Add changes to changelog.
    newFirstLine="$(date +"${GENERATION_DATE_LINE_PREFIX}${GENERATION_DATE_FORMAT}${GENERATION_DATE_LINE_SUFFIX}")\n"
    oldContent=$(cat ${DEVELOP_CHANGELOG_FILE})
    dateLine="## "$(date +${SHOWED_CHANGE_DATE_PATTERN})"\n"
    echo -e "${newFirstLine}${dateLine}${changes}\n\n${oldContent}" > ${DEVELOP_CHANGELOG_FILE}

    # Commit and push changelog.
    git config --global user.name "${GITLAB_USER_NAME}"
    git config --global user.email "${GITLAB_USER_EMAIL}"
    git add ${DEVELOP_CHANGELOG_FILE}
    git commit -m "Add history files" ${DEVELOP_CHANGELOG_FILE}

    git push http://gitlab-ci-token:${PUSH_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git HEAD:master || exit 1

    echo "Changelog was updated!"
else
    echo -e "\e[33m\nChangelog wasn't updated! Changes wasn't found! \e[0m"
fi
